# Author: Chetan Munigangappa
# Webpage: https://chetangangappa.codeberg.page
# Business Analyst: Shreyas Kumaraswamy
# License: GPLv2
# Work Type: Freelance with the condition to open-source the code

# Assumptions:
# * This programs makes the assumption that the CSV input contains data for only one year (defined in requirements)

import pandas as pd  # used for handling dataframe
import numpy as np  # used for vector manipulations
import uuid  # used for id generation for unique dealers
import os.path  # used for file operations
import xlsxwriter  # used to write xlsx files


# Handle all dealer related operations here
class Dealer:
    id: uuid
    name: str
    target: float

    def __init__(self, name: str, target: float):
        self.id = uuid.uuid4()
        self.name = name
        self.target = target

    def __str__(self):
        return "\nID: " + str(self.id) + "\nName: " + self.name + "\nTarget:" + str(self.target)

    # Function: Get Dealer Names
    @staticmethod
    def get_unique_dealers(df, column_name: str):
        return df[column_name].unique()

    # Function: Collect Dealer Target Info From User
    # Note: Later to be replaced by CSV file in the future, hence makeshift
    @staticmethod
    def get_dealer_target_info(dealer_names):
        dealer_dict = {}
        for dealer in dealer_names:
            target = float(input('Enter ' + dealer + ' target: '))
            new_dealer = Dealer(dealer, target)
            dealer_dict[new_dealer.id] = new_dealer
        return dealer_dict

    # Function: Filter by Deal Owners
    @staticmethod
    def filter_by_deal_owners(df, dealer_store: dict, column_name: str):
        df_filter = []
        for dealer in dealer_store.values():
            df_filter.append([dealer, df.loc[df[column_name].eq(dealer.name)]])
        return df_filter


# Handle all commission related calculations here
class Commission:
    tranche_classification_targets: [float]
    platform_percentages: [float]  # should be one more than classification_targets
    service_percentages: [float]  # should be one more than classification_targets

    def __init__(self, tranche_classification_targets: [float], platform_percentages: [float],
                 service_percentages: [float]):
        self.tranche_classification_targets = tranche_classification_targets
        self.platform_percentages = platform_percentages
        self.service_percentages = service_percentages

    # Function: Calculate the amount targets for each tranches
    def _calculate_tranche_classification_amounts(self, target: float):
        return np.array(self.tranche_classification_targets) * target

    # Function: Return the amount splits for different tranches
    def _calculate_tranche_amount_remaining(self, target, input_amount: float):
        tranche_amounts = self._calculate_tranche_classification_amounts(target)
        remaining_amount = None
        tranche = None
        last_tranche = False
        for i in range(0, len(tranche_amounts)):
            if input_amount < tranche_amounts[i]:
                tranche = i
                break
            elif input_amount == tranche_amounts[i]:
                tranche = i + 1
                break
        # escape condition for last tranche
        if tranche is None or tranche == len(tranche_amounts):
            tranche = len(tranche_amounts)
            last_tranche = True
        # check if it is not the last tranche, calculate the remaining amount
        if not last_tranche:
            remaining_amount = tranche_amounts[tranche] - input_amount
        return tranche, remaining_amount, last_tranche

    # Function: Calculate the commission for the given tranche and amount depending on sale type
    # returns tranche string and commission amount
    def _simple_commission_calculation(self, tranche: int, sale_type: str, amount: float):
        tranche_output_string: str
        calculated_commission_value: float

        tranche_output_string = str(tranche + 1)  # since tranche starts from 1 for all purposes other than programming
        if sale_type == 'Platform':
            calculated_commission_value = (self.platform_percentages[tranche] / 100.0) * amount
        # elif sale_type == 'Services':
        else:
            calculated_commission_value = (self.service_percentages[tranche] / 100.0) * amount
        return tranche_output_string, calculated_commission_value

    # Function: Calculates the dealer commission for the current sale
    def commission_calculation(self, target: float, cumulative_sales_amount: float, sales_amount: float,
                               sale_type: str):
        tranche_output_string: str = ''
        calculated_commission_value: float = 0.0
        remaining_sales_amount = sales_amount
        current_cumulative_sales_amount = cumulative_sales_amount
        while remaining_sales_amount >= 0:
            # check the current tranche the sales are on
            current_tranche, remaining_tranche_amount, last_tranche = \
                self._calculate_tranche_amount_remaining(target, current_cumulative_sales_amount)
            if last_tranche or remaining_tranche_amount >= remaining_sales_amount:
                current_tranche_output_string, current_calculated_commission_value = \
                    self._simple_commission_calculation(current_tranche, sale_type, sales_amount)
                tranche_output_string += current_tranche_output_string
                calculated_commission_value += current_calculated_commission_value
                break
            current_tranche_output_string, current_calculated_commission_value = self._simple_commission_calculation(
                current_tranche, sale_type, remaining_tranche_amount)
            tranche_output_string += current_tranche_output_string
            calculated_commission_value += current_calculated_commission_value
            # update remaining sales amount and cumulative sales amount
            remaining_sales_amount -= remaining_tranche_amount
            current_cumulative_sales_amount += remaining_tranche_amount
        return tranche_output_string, calculated_commission_value


# Handle all report related operations here
class Report:
    quarter_column_name = 'Quarter'

    # Function: Insert Quarters Column
    @staticmethod
    def insert_quarters_column(df, source_column_name: str):
        df[Report.quarter_column_name] = df[source_column_name].dt.quarter
        return df

    # Function: Sort the data by dates
    @staticmethod
    def sort_by_date(df, column_name: str):
        df.sort_values(by=column_name, ascending=True, inplace=True)
        # Alternative method for sorting for future reference
        # df.set_index(column_name, drop=False, append=False, inplace=True)
        # df = df.sort_index()
        return df

    # Function: Filter by Deal Type and Quarters
    # Assumes the dataframe passed has only one dealer information
    @staticmethod
    def _filter_by_quarters_deal_type(df, deal_type_column_name: str):
        df_filter = []
        type_platform = "Platform"
        type_services = "Services"
        for quarter in range(1, 5):
            df_filter.append(
                [type_platform, quarter,
                 df.loc[df[Report.quarter_column_name].eq(quarter) & df[deal_type_column_name].eq(type_platform)]])
        for quarter in range(1, 5):
            df_filter.append(
                [type_services, quarter,
                 df.loc[df[Report.quarter_column_name].eq(quarter) & df[deal_type_column_name].eq(type_services)]])
        return df_filter

    # Function: Add Tranche, Calculated Commission for each dealer
    # Assumes the dataframe passed has only one dealer information
    @staticmethod
    def insert_tranche_and_commission_calculations(df, deal_amount_column_name: str, deal_type_column_name: str,
                                                   dealer: Dealer, commission_instance: Commission):
        calculated_tranche_col_name = 'Tranche'
        calculated_commission_amount_col_name = 'Commission Amount'
        calculated_cumulative_sales_amount_col_name = 'Cumulative Sales Amount'
        df[calculated_tranche_col_name]: [str]
        df[calculated_commission_amount_col_name]: [float]
        df[calculated_cumulative_sales_amount_col_name]: [float]

        calculated_tranches: [str] = []
        calculated_percentage_amount: [float] = []
        calculated_cumulative_sales_amount: [float] = []
        last_cumulative_sales_amount: float = 0.0
        for index, row in df.iterrows():
            current_deal_amount = row[deal_amount_column_name]
            current_sale_type = row[deal_type_column_name]
            current_tranche, current_commission = Commission.commission_calculation(commission_instance, dealer.target,
                                                                                    last_cumulative_sales_amount,
                                                                                    current_deal_amount,
                                                                                    current_sale_type)
            # update values
            last_cumulative_sales_amount += current_deal_amount
            calculated_tranches.append(current_tranche)
            calculated_cumulative_sales_amount.append(last_cumulative_sales_amount)
            calculated_percentage_amount.append(current_commission)
        df[calculated_tranche_col_name] = calculated_tranches
        df[calculated_commission_amount_col_name] = calculated_percentage_amount
        df[calculated_cumulative_sales_amount_col_name] = calculated_cumulative_sales_amount
        return df

    # Function: Create Report File
    @staticmethod
    def create_report(df, dealer: Dealer, deal_type_column_name: str):
        file = 'CP ' + dealer.name + '.xlsx'
        # check if file exists and delete file if exists
        if os.path.exists(file):
            os.remove(file)
        writer = pd.ExcelWriter(file, engine='xlsxwriter')
        Report._export_summary_to_excel(writer, df)  # create the summary page for the particular dealer
        df_quarter_deal_type_filtered_dfs = Report._filter_by_quarters_deal_type(df, deal_type_column_name)
        for df_quarter_deal_type_filtered_df in df_quarter_deal_type_filtered_dfs:
            Report._export_quarter_type_summary_to_excel(writer, df_quarter_deal_type_filtered_df[2],
                                                         df_quarter_deal_type_filtered_df[0],
                                                         df_quarter_deal_type_filtered_df[1])
        writer.save()

    # Function: Export Summary Dataframe to Excel
    @staticmethod
    def _export_summary_to_excel(writer, df):
        sheet_name = 'Summary'
        df.to_excel(writer, sheet_name=sheet_name, startrow=1, header=False, index=False)
        workbook = writer.book
        worksheet = writer.sheets[sheet_name]
        (max_row, max_col) = df.shape
        column_settings = [{'header': column} for column in df.columns]
        worksheet.add_table(0, 0, max_row, max_col - 1, {'columns': column_settings})
        worksheet.set_column(0, max_col - 1, 20)

    # Function: Export Quarterly and Type Summary Dataframe to Excel
    @staticmethod
    def _export_quarter_type_summary_to_excel(writer, df, deal_type: str, quarter: int):
        column_names = ['Deal Name', 'Amount', 'Commission Amount']  # Note: this is hardcoded right now, as it is WIP
        sheet_name = deal_type + '_Q' + str(quarter)
        df.to_excel(writer, sheet_name=sheet_name, startrow=1, header=False, index=False)
        worksheet = writer.sheets[sheet_name]
        (max_row, max_col) = df.shape
        column_settings = [{'header': column} for column in df.columns]
        worksheet.add_table(0, 0, max_row, max_col - 1, {'columns': column_settings})
        worksheet.set_column(0, max_col - 1, 20)


# Main Program

# Function: Collect Input File
def collect_input_file_name_from_user():
    while True:
        path = input("Enter File Name: ")
        if not os.path.isfile(path):
            print("The entered file path is invalid/not found, please try again")
            continue
        input("Warning: Any obsolete/older commission calculation files will be replaced." +
              " Exit the program and take a backup if required")
        return os.path.abspath(path)


# Variables
date_column: str = 'Close Date'
dealer_name_column: str = 'Deal owner'
deal_type_column: str = 'Product Type'
deal_amount_column: str = 'Amount'

tranche_classification_percentages = [0.3333, 0.6666, 1]
commission_rates_platform = [10.8, 13.5, 16.8, 18]
commission_rates_services = [8.64, 10.8, 12.96, 14.4]

# Program Steps
# Step 1: Take User Input
file_name = collect_input_file_name_from_user()  # gets the absolute path of the file for standardization
# Step 2: Read the File & Create a Dataframe
# df = pd.read_csv(file_name, delimiter=',', engine='python', parse_dates=[date_column], dayfirst=True)
df = pd.read_csv(file_name, delimiter=',', parse_dates=[date_column], dayfirst=True)
# Step 3: Get Unique Dealers & Their Targets
dealers = Dealer.get_unique_dealers(df, dealer_name_column)
dealer_store = Dealer.get_dealer_target_info(dealers)  # dictionary with dealer objects and uuid for each dealer
# Step 4: Generate Quarters Columns
df = Report.insert_quarters_column(df, date_column)
# Step 5: Organize the columns by date values
df = Report.sort_by_date(df, date_column)
# Step 6: Get a df for each dealer
dealer_dfs = Dealer.filter_by_deal_owners(df, dealer_store, dealer_name_column)
# Step 7: Calculate commission for each of the dealer dfs
calculated_dealer_dfs = []
commission = Commission(tranche_classification_targets=tranche_classification_percentages,
                        service_percentages=commission_rates_services, platform_percentages=commission_rates_platform)
for dealer_df in dealer_dfs:
    calculated_dealer_dfs.append([dealer_df[0], Report.insert_tranche_and_commission_calculations(dealer_df[1],
                                                                                                  deal_amount_column,
                                                                                                  deal_type_column,
                                                                                                  dealer_df[0],
                                                                                                  commission)])
# Step 8: Exporting Dataframe to excel
for calculated_dealer_df in calculated_dealer_dfs:
    Report.create_report(calculated_dealer_df[1], calculated_dealer_df[0], deal_type_column)
